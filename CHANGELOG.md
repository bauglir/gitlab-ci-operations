# Changelog

## [13.1.3](https://gitlab.com/ethima/gitlab-ci/compare/v13.1.2...v13.1.3) (2025-03-03)


### Bug Fixes

* **deps:** update dependency prettier to v3.5.3 ([b6c38ef](https://gitlab.com/ethima/gitlab-ci/commit/b6c38ef100899a9216b3acfbdb2620f24453cccd))

## [13.1.2](https://gitlab.com/ethima/gitlab-ci/compare/v13.1.1...v13.1.2) (2025-02-23)


### Bug Fixes

* **deps:** update dependency prettier to v3.5.2 ([f974aa3](https://gitlab.com/ethima/gitlab-ci/commit/f974aa316ae725cbd3cf3464334587927650820c))

## [13.1.1](https://gitlab.com/ethima/gitlab-ci/compare/v13.1.0...v13.1.1) (2025-02-16)


### Bug Fixes

* **deps:** update dependency prettier to v3.5.1 ([422d3c3](https://gitlab.com/ethima/gitlab-ci/commit/422d3c35581373b4b4cba2364d595d355a640fc6))

# [13.1.0](https://gitlab.com/ethima/gitlab-ci/compare/v13.0.0...v13.1.0) (2025-02-10)


### Features

* **deps:** update dependency prettier to v3.5.0 ([f1cb952](https://gitlab.com/ethima/gitlab-ci/commit/f1cb952f342bf615c5948a082964f854a0abbc97))

# [13.0.0](https://gitlab.com/ethima/gitlab-ci/compare/v12.0.0...v13.0.0) (2024-11-20)


* build(deps)!: update dependency ethima/dependency-management to v3 ([8b69910](https://gitlab.com/ethima/gitlab-ci/commit/8b69910ac6f03edb7d5ab2f382d2bd8c0c5e5400))


### Features

* **deps:** update dependency ethima/semantic-release to v9.1.0 ([954bf1c](https://gitlab.com/ethima/gitlab-ci/commit/954bf1cb1f190e4b1dd98a2cdd09623a1fac89be))


### BREAKING CHANGES

* Update to Renovate v39 for dependency management. Clear
any caches in case of user filesystem conflicts. For full details on
potentially necessary configuration changes see
https://github.com/renovatebot/renovate/releases/tag/39.0.0.

# [13.0.0-rc.1](https://gitlab.com/ethima/gitlab-ci/compare/v12.1.0-rc.1...v13.0.0-rc.1) (2024-11-20)


* build(deps)!: update dependency ethima/dependency-management to v3 ([8b69910](https://gitlab.com/ethima/gitlab-ci/commit/8b69910ac6f03edb7d5ab2f382d2bd8c0c5e5400))


### BREAKING CHANGES

* Update to Renovate v39 for dependency management. Clear
any caches in case of user filesystem conflicts. For full details on
potentially necessary configuration changes see
https://github.com/renovatebot/renovate/releases/tag/39.0.0.

# [12.1.0-rc.1](https://gitlab.com/ethima/gitlab-ci/compare/v12.0.0...v12.1.0-rc.1) (2024-11-20)


### Features

* **deps:** update dependency ethima/semantic-release to v9.1.0 ([954bf1c](https://gitlab.com/ethima/gitlab-ci/commit/954bf1cb1f190e4b1dd98a2cdd09623a1fac89be))

# [12.0.0](https://gitlab.com/ethima/gitlab-ci/compare/v11.0.0...v12.0.0) (2024-11-01)


* build(deps)!: update dependency ethima/dependency-management to v2 ([ab05e7f](https://gitlab.com/ethima/gitlab-ci/commit/ab05e7fbdc818d91968540a6f664ce705b2c3e9f))


### BREAKING CHANGES

* Update to Renovate v38. For details on the respective
necessary configuration changes renovate-bot/renovate-runner!2863.

# [11.0.0](https://gitlab.com/ethima/gitlab-ci/compare/v10.0.0...v11.0.0) (2024-11-01)


* build(deps)!: update dependency ethima/semantic-release to v9 ([0c61659](https://gitlab.com/ethima/gitlab-ci/commit/0c61659733dbd8bada744a914be9f2e23ee814cd))


### BREAKING CHANGES

* The version of the `semantic-release` tooling this
release ships with (v24.2.0), leverages the `commit-analyzer` and
`release-notes-generator` plugins which both expect to be used with the
latest major versions of conventional-changelog packages. If you are
installing any of these packages in addition to semantic-release, be
sure to update them as well.

# [10.0.0](https://gitlab.com/ethima/gitlab-ci/compare/v9.0.1...v10.0.0) (2024-02-20)


### Bug Fixes

* **semantic-release:** exclude the semantic release process when requested ([74b26b2](https://gitlab.com/ethima/gitlab-ci/commit/74b26b227f0ea6cac23c37651c019fd0e350f8ec))


* build(deps)!: update dependency ethima/semantic-release to v8 ([8a2faa3](https://gitlab.com/ethima/gitlab-ci/commit/8a2faa35d1e436c71f60b42bcea7c0a58f10065c))


### BREAKING CHANGES

* The `ethima/semantic-release@8` process depends on
`@ethima/semantic-release-configuration@8` and `semantic-release@23`
which in turn depend on `cosmiconfig@9`. Primarily, this means that if a
project is using a `config.js` file, or an equivalent such as
`config.json` or `config.ts`, to configure `cosmiconfig` itself, the
file needs to be moved into a `.config` directory in the root of the
project.

See `cosmiconfig`'s ["Usage for end users"
documentation][cosmiconfig-meta-config-url] for details and its [v9.0.0
release notes][cosmiconfig-v9-release-notes-url].

[cosmiconfig-meta-config-url]: https://github.com/cosmiconfig/cosmiconfig/blob/a5a842547c13392ebb89a485b9e56d9f37e3cbd3/README.md#usage-for-end-users
[cosmiconfig-v9-release-notes-url]: https://github.com/cosmiconfig/cosmiconfig/releases/tag/v9.0.0

## [9.0.1](https://gitlab.com/ethima/gitlab-ci/compare/v9.0.0...v9.0.1) (2024-01-05)


### Bug Fixes

* **deps:** update dependency ethima/semantic-release to v7.0.1 ([b982765](https://gitlab.com/ethima/gitlab-ci/commit/b98276534073100ddfcd881dc2e6831322af3469))

# [9.0.0](https://gitlab.com/ethima/gitlab-ci/compare/v8.3.1...v9.0.0) (2024-01-05)


* feat(deps)!: update dependency ethima/semantic-release to v7 ([b9c0f63](https://gitlab.com/ethima/gitlab-ci/commit/b9c0f631c9ae66593ad1588f9f64800f00dd578c))


### BREAKING CHANGES

* BREAKING CHANGE: The update to
[@ethima/semantic-release@7](https://gitlab.com/ethima/semantic-release/-/releases/v7.0.0)
includes a change to the commit type used for release commits in the
underlying configuration that is used. This type has been changed from
`chore` to `build`. This is only a breaking change for those with
additional workflows on top of the semantic release workflow provided by
this project relying on the structure of these commit messages.

## [8.3.1](https://gitlab.com/ethima/gitlab-ci/compare/v8.3.0...v8.3.1) (2023-10-29)


### Bug Fixes

* **deps:** update dependency ethima/semantic-release to v6.1.2 ([3030730](https://gitlab.com/ethima/gitlab-ci/commit/3030730b85c022ce1c99d7090edf5534a0ee68dd))

# [8.3.0](https://gitlab.com/ethima/gitlab-ci/compare/v8.2.0...v8.3.0) (2023-10-29)


### Features

* **process:** define the Ethima Git actor by default ([ef857a1](https://gitlab.com/ethima/gitlab-ci/commit/ef857a13655e04c06bc0fc09a2d124159d58924a))
* **variables:** define a common Git actor ([2282c59](https://gitlab.com/ethima/gitlab-ci/commit/2282c599e941af1140da01b6c4f2aac03542eccf))

# [8.2.0](https://gitlab.com/ethima/gitlab-ci/compare/v8.1.1...v8.2.0) (2023-10-25)


### Features

* **deps:** update node.js to v20 ([10ad6c0](https://gitlab.com/ethima/gitlab-ci/commit/10ad6c0d29d8b3d50eb43bf4cc5c899ac2ec348d))

## [8.1.1](https://gitlab.com/ethima/gitlab-ci/compare/v8.1.0...v8.1.1) (2023-07-28)


### Bug Fixes

* **deps:** update dependency ethima/semantic-release to v6 ([87299ae](https://gitlab.com/ethima/gitlab-ci/commit/87299ae308f498a2ea262b585d9aecec2970d650))

# [8.1.0](https://gitlab.com/ethima/gitlab-ci/compare/v8.0.0...v8.1.0) (2023-07-28)


### Features

* **deps:** update dependency ethima/dependency-management to v1.4.0 ([ec838e8](https://gitlab.com/ethima/gitlab-ci/commit/ec838e8ceb8969b674aa4a097c95d4c431a51367))

# [8.0.0](https://gitlab.com/ethima/gitlab-ci/compare/v7.0.0...v8.0.0) (2023-07-28)


### Bug Fixes

* **workflows:** trigger the `Primary Release Branch (Push)` scenario in its default case ([2da7716](https://gitlab.com/ethima/gitlab-ci/commit/2da77164925e4e747b5fb6ebe0b1825601e2fd0c))

# [7.0.0](https://gitlab.com/ethima/gitlab-ci/compare/v6.0.0...v7.0.0) (2023-07-27)


### Bug Fixes

* **workflows:** add `Primary Release Branch (Push)` trigger for the default case ([a0e8cf1](https://gitlab.com/ethima/gitlab-ci/commit/a0e8cf1d44fbee8c5f5bc386acb91e77371f513e))

# [6.0.0](https://gitlab.com/ethima/gitlab-ci/compare/v5.2.1...v6.0.0) (2023-07-11)


### Features

* **workflows:** add a `Maintenance Branch (Push)` scenario ([7e9381d](https://gitlab.com/ethima/gitlab-ci/commit/7e9381d195b160026527cfddd83f50b14ef213e8))
* **workflows:** add a `Prerelease Branch (Push)` scenario ([bcebfb0](https://gitlab.com/ethima/gitlab-ci/commit/bcebfb0a049c8909b0d55b6f2c683d5da84f0c53))
* **workflows:** add a `Prerelease` workflow scenario ([da4a24d](https://gitlab.com/ethima/gitlab-ci/commit/da4a24d2f73ef237036767746195a406b1aa648d))
* **workflows:** add a `Primary Release Branch (Push)` scenario ([4db30de](https://gitlab.com/ethima/gitlab-ci/commit/4db30dec3e83c864a01dad3eca3ba11a489bc1d8))

## [5.2.1](https://gitlab.com/ethima/gitlab-ci/compare/v5.2.0...v5.2.1) (2023-06-11)


### Bug Fixes

* **deps:** update dependency ethima/dependency-management to v1.3.1 ([edd9974](https://gitlab.com/ethima/gitlab-ci/commit/edd9974b2c90486f2709399638493db6e48d99ee))

# [5.2.0](https://gitlab.com/ethima/gitlab-ci/compare/v5.1.0...v5.2.0) (2023-05-10)


### Features

* **workflows:** add a `Release` workflow scenario ([d3e31c5](https://gitlab.com/ethima/gitlab-ci/commit/d3e31c5e12d871796947c2b6f6b1538fc391fae9))

# [5.1.0](https://gitlab.com/ethima/gitlab-ci/compare/v5.0.0...v5.1.0) (2023-05-05)


### Features

* add dependency management process ([e1528f6](https://gitlab.com/ethima/gitlab-ci/commit/e1528f636f4ca3630f08fb90b2e1028663a506bd))

# [5.0.0](https://gitlab.com/ethima/gitlab-ci/compare/v4.1.0...v5.0.0) (2023-05-05)


* feat(process)!: remove the deprecated `Default Branch` workflow ([8c14afd](https://gitlab.com/ethima/gitlab-ci/commit/8c14afd121f134e2015dceef8b31fc0876952ba7))


### BREAKING CHANGES

* The, previously deprecated, `Default Branch` workflow
is removed in favor of the `Default Branch (Push)` workflow. Update all
references to the new name, e.g. in `workflow.rules` entries and when
extending the workflow.

# [4.1.0](https://gitlab.com/ethima/gitlab-ci/compare/v4.0.0...v4.1.0) (2023-05-05)


### Bug Fixes

* **workflows:** only trigger the `Default Branch` scenario on pushes ([4a26def](https://gitlab.com/ethima/gitlab-ci/commit/4a26def4b4a512c3c986292d59465f0cb87ee580)), closes [/gitlab.com/ethima/gitlab-ci/-/blob/cc5e03cd95a617fba3d38a7926bfc60375557bd6/.gitlab/ci/process.yml#L25-27](https://gitlab.com//gitlab.com/ethima/gitlab-ci/-/blob/cc5e03cd95a617fba3d38a7926bfc60375557bd6/.gitlab/ci/process.yml/issues/L25-27) [/gitlab.com/ethima/dependency-management/-/blob/48ef468fa18a6af7ef15df5115551128c7fb206d/.gitlab-ci.yml#L26-29](https://gitlab.com//gitlab.com/ethima/dependency-management/-/blob/48ef468fa18a6af7ef15df5115551128c7fb206d/.gitlab-ci.yml/issues/L26-29)


### Features

* **workflows:** rename the `Default Branch` to `Default Branch (Push)` ([a52442d](https://gitlab.com/ethima/gitlab-ci/commit/a52442db6c401b6388ed28820bac54839a9d2391))

# [4.0.0](https://gitlab.com/ethima/gitlab-ci/compare/v3.2.0...v4.0.0) (2023-04-26)


* feat(deps)!: update dependency `ethima/semantic-release` to v4 ([54cf46e](https://gitlab.com/ethima/gitlab-ci/commit/54cf46e9e92259645edb8ca74ffc5fa0381c0689)), closes [/gitlab.com/ethima/semantic-release-configuration/-/blob/c420d14c7e66d6227a04e54ad116fd2654f8d215/CHANGELOG.md#400-2023-04-25](https://gitlab.com//gitlab.com/ethima/semantic-release-configuration/-/blob/c420d14c7e66d6227a04e54ad116fd2654f8d215/CHANGELOG.md/issues/400-2023-04-25)


### BREAKING CHANGES

* The tokens used by the
`@ethima/semantic-release-configuration`, which is included in the
`ethima/semantic-release` project, to annotate versioned templates have
changed[^1]. Given that these tokens are exposed directly through this
project it necessitates a broken release.

The previously used `<!-- START_VERSIONED_TEMPLATE` and `<!--
END_VERSIONED_TEMPLATE -->` tokens are replaced with the
`BEGIN_VERSIONED_TEMPLATE` and `END_VERSIONED_TEMPLATE_REPLACEMENT`
tokens respectively. The `END_VERSIONED_TEMPLATE` token now indicates
the end of the template, not the end of the replaced content.

# [3.2.0](https://gitlab.com/ethima/gitlab-ci/compare/v3.1.0...v3.2.0) (2023-02-20)


### Features

* **deps:** update dependency ethima/semantic-release to v3.2.0 ([ad2884f](https://gitlab.com/ethima/gitlab-ci/commit/ad2884f26079e2c0597055e2c848f648512cf0c0))

# [3.1.0](https://gitlab.com/ethima/gitlab-ci/compare/v3.0.0...v3.1.0) (2023-01-19)


### Features

* **prettier:** provide prefilled variable hints for manual pipelines ([58e0d77](https://gitlab.com/ethima/gitlab-ci/commit/58e0d77df182a9bbf5cbd55456df4e94fe06c764))
* **workflows:** add `Manual` scenario for manually run pipelines ([678d3dc](https://gitlab.com/ethima/gitlab-ci/commit/678d3dcc798942ae7c0dea5e38abffd3f71b9dc5))

# [3.0.0](https://gitlab.com/ethima/gitlab-ci/compare/v2.1.0...v3.0.0) (2023-01-19)


### chore

* **deps:** update dependency ethima/semantic-release to v3 ([05287f9](https://gitlab.com/ethima/gitlab-ci/commit/05287f9bb5baf819b9def2c39595af4488a78b95))


### BREAKING CHANGES

* **deps:** The ethima/semantic-release-configuration> has switched
to the [Conventional Commits](https://conventionalcommits.org) preset
for the semantic release tooling. This should be mostly backwards
compatible with the previously used
[`angular`][semantic-release-preset-angular] preset, but is considered a
breaking change. This propagates through this process as the
configuration is the default for it through the ethima/semantic-release>
process.

[semantic-release-preset-conventionalcommits]: https://github.com/conventional-changelog/conventional-changelog/tree/master/packages/conventional-changelog-conventionalcommits
[semantic-release-preset-angular]: https://github.com/conventional-changelog/conventional-changelog/tree/master/packages/conventional-changelog-angular

# [2.1.0](https://gitlab.com/ethima/gitlab-ci/compare/v2.0.2...v2.1.0) (2023-01-10)


### Features

* **workflows:** add `Schedule` scenario targeting scheduled pipelines ([6c17a22](https://gitlab.com/ethima/gitlab-ci/commit/6c17a22a56d6d952f62dd5e2aab4225b2f4826fd))

## [2.0.2](https://gitlab.com/ethima/gitlab-ci/compare/v2.0.1...v2.0.2) (2023-01-07)


### Bug Fixes

* **deps:** update dependency ethima/semantic-release to v2.0.1 ([422c1b6](https://gitlab.com/ethima/gitlab-ci/commit/422c1b60337dac4c7bc2fe2004d7c4b2368ab898))

## [2.0.1](https://gitlab.com/ethima/gitlab-ci/compare/v2.0.0...v2.0.1) (2022-12-30)


### Bug Fixes

* migrate to ethima/semantic-release> ([3a6edfa](https://gitlab.com/ethima/gitlab-ci/commit/3a6edfad4f3a760518f803943f890100a76c00f9))

# [2.0.0](https://gitlab.com/ethima/gitlab-ci/compare/v1.3.1...v2.0.0) (2022-12-28)


* feat!: migrate to the @ethima organization ([07d488e](https://gitlab.com/ethima/gitlab-ci/commit/07d488e75cbda28604c0336e13e7275cdf42255c))


### BREAKING CHANGES

* The project has been migrated from @bauglir's personal
namespace to the @ethima namespace, which is intended to host a variety
of process-related projects, and renamed accordingly. It is now more
succinctly known as ethima/gitlab-ci>.

## [1.3.1](https://gitlab.com/ethima/gitlab-ci/compare/v1.3.0...v1.3.1) (2022-12-28)

### Bug Fixes

- configure pipeline names using a `PROCESS_NAME` variable ([1a977c6](https://gitlab.com/ethima/gitlab-ci/commit/1a977c60df706c09535f78aced46cc92cf722214))

# [1.3.0](https://gitlab.com/ethima/gitlab-ci/compare/v1.2.0...v1.3.0) (2022-12-23)

### Features

- **process:** include the semantic release process by default ([07a411c](https://gitlab.com/ethima/gitlab-ci/commit/07a411c468733aae915646c0ba3ab30b3e783976))

# [1.2.0](https://gitlab.com/ethima/gitlab-ci/compare/v1.1.0...v1.2.0) (2022-11-16)

### Bug Fixes

- **process:** stop code formatting verification for the default branch ([f46e1de](https://gitlab.com/ethima/gitlab-ci/commit/f46e1defcf720e31d1c72355324e9060e5edc2bd))

### Features

- **process:** improve reuse of the process workflow definition ([75fea39](https://gitlab.com/ethima/gitlab-ci/commit/75fea393a1634b67816ff559e54c914878dbd2dd))
- **workflows:** add common workflows that can be extended across processes ([d2f149c](https://gitlab.com/ethima/gitlab-ci/commit/d2f149c23435aaab6b6a48447e20673a963c6e20))

# [1.1.0](https://gitlab.com/ethima/gitlab-ci/compare/v1.0.0...v1.1.0) (2022-11-08)

### Features

- **code-formatting:** verify code formatting through Prettier ([87fee70](https://gitlab.com/ethima/gitlab-ci/commit/87fee709a0718730119a91c36be044b18ff7e6b8))
- **process:** add `process` pipeline for common tasks ([0be4f67](https://gitlab.com/ethima/gitlab-ci/commit/0be4f67ceb845c3fb4f9eacf8797039c619b8da1))

# 1.0.0 (2022-11-08)

### Bug Fixes

- **gitlab-pages:** publish from the 'default branch' gated by a variable ([6fac0f5](https://gitlab.com/ethima/gitlab-ci/commit/6fac0f560cc81c4b64aee7086ea21a193bf722b0))
- **gitlab-pages:** trigger on non-empty `public` folders ([e0171d6](https://gitlab.com/ethima/gitlab-ci/commit/e0171d62edf7fd89889a4920bea30014d8e0dbb9))
- **gitlab-pages:** unquote the default `GITLAB_PAGES_DEPLOYMENT_BRANCH` variable definition ([757f307](https://gitlab.com/ethima/gitlab-ci/commit/757f307888eb7e6fe635c1a4f95de97aa2168f63))
- **stages:** expose a `COMMON_CI_STAGES` variable to enable detection ([3607f1a](https://gitlab.com/ethima/gitlab-ci/commit/3607f1afd05d2c9015da4d95cc530daa28f098eb))

### Features

- add common CI `stage` definitions ([5733b94](https://gitlab.com/ethima/gitlab-ci/commit/5733b949fa34e556d7ed3a6718d27dcfc9ded8ae))
- **gitlab-pages:** enable deployment of statically defined sites ([e5daf52](https://gitlab.com/ethima/gitlab-ci/commit/e5daf5272feca81aa3450b12a6945913cda701ec))
