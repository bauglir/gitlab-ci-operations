# GitLab CI

Common definitions and processes for GitLab CI supporting projects directly and
facilitating the creation of more complex processes.

## Usage

Include the [process](#process) definition into a project's `/.gitlab-ci.yml`
file using

<!-- BEGIN_VERSIONED_TEMPLATE

```yml
include:
  - file: "/.gitlab/ci/process.yml"
    project: "ethima/gitlab-ci"
    ref: "v__NEXT_SEMANTIC_RELEASE_VERSION__"
```

-->

```yml
include:
  - file: "/.gitlab/ci/process.yml"
    project: "ethima/gitlab-ci"
    ref: "v13.1.3"
```

<!-- END_VERSIONED_TEMPLATE_REPLACEMENT -->

This process includes the ethima/dependency-management> and
ethima/semantic-release> processes by default.

The dependency management process can be run from manual pipelines or on a
schedule, either for the enrolled project or for a group of projects. See [the
_Usage_ section in the process' documentation][dependency-management-usage-url]
for details on these and more advanced scenarios.

### Configuration

Different components of the Ethima processes require various access tokens to
perform tasks on specific platforms. Examples include pulling and pushing code
from projects and creating releases on GitLab, retrieving release notes from
GitHub, publishing packages to NPM, etc. The environment variables, used to
pass these tokens to the underlying tooling, vary wildly in name. To make it
easier to configure these tokens across Ethima processes, they adopt a
convention for specying platform-specific tokens, e.g. `ETHIMA_GITLAB_TOKEN`,
`ETHIMA_GITHUB_TOKEN`, `ETHIMA_NPM_TOKEN`, etc. The necessary scopes that
should be applied to these tokens are explained in further detail in the
documentation of the different processes, e.g. for the [dependency management
process][dependency-management-usage-url] and the [semantic release
configuration][semantic-release-configuration-usage-url].

Specifying tokens at the platform level is convenient, but it may mean that the
tokens have more privileges than strictly necessary for specific tasks. To
facilitate more granular control over the privileges of tokens and for which
purpose they are considered suitable, a second layer of variables is used which
default to the platform specific tokens, e.g.
`ETHIMA_SEMANTIC_RELEASE_GITLAB_TOKEN`,
`ETHIMA_DEPENDENCY_MANAGEMENT_GITHUB_TOKEN`, etc. These tokens are eventually
applied to the tooling specific environment variables.

## [Process](/.gitlab/ci/process.yml)

The [`process`](/.gitlab/ci/process.yml) creates pipelines that include jobs
applying to projects hosted on GitLab that are (mostly) technology stack
agnostic. In other words, these jobs apply to JavaScript/TypeScript projects,
GitLab CI process projects such as this one, Julia projects, etc. Its purpose
is to serve as a basis for more specific processes.

Which [jobs](#jobs) are included in pipelines created through the process
depends on how the process was triggered. The following table outlines which
jobs are included for which [workflow scenarios](#workflows):

| Job                                                                           | Scenario        |
| ----------------------------------------------------------------------------- | --------------- |
| [`Verify Code Formatting (Prettier)`](#code-formatting-verification-prettier) | `Merge Request` |

Jobs that are disabled for a given workflow can be enabled by setting their
corresponding `ENABLE_*` environment variable to `"true"` and vice versa.
Taking into account that jobs should typically be disabled by default.

The process includes the ethima/semantic-release> project by default. This can
be disabled by setting the `ENABLE_SEMANTIC_RELEASE` environment variable in CI
to any value other than `"true"`. Note that due to the variable being used to
conditionally `include` the process, the variable can only be specified through
[the methods outlined in GitLab's
documentation](https://docs.gitlab.com/16.9/ee/ci/yaml/includes.html#use-variables-with-include).

## Jobs

### [Code Formatting Verification (Prettier)](/.gitlab/ci/verify-code-formatting-prettier.yml)

The
[`.gitlab/ci/verify-code-formatting-prettier.yml`](/.gitlab/ci/verify-code-formatting-prettier.yml)
file defines a CI job that runs the opinionated code formatter
[Prettier][prettier-url] against a project's codebase. If the codebase does not
adhere to Prettier's opinions the job fails.

Prettier has a number of [configuration
options][prettier-configuration-options-url]. By default, the job will look for
a `.prettierrc` file defining these options and use Prettier's defaults if no
such file is available. In case [a different configuration
file][prettier-configuration-file-url] is used, its path can be configured
using the `PRETTIER_RC` environment variable in CI. Using [GitLab's _file_
environment variables][gitlab-file-variable-url], the configuration can also be
done through GitLab's UI. Configuration through a `prettier` property in a
JavaScript project's `package.json` is not supported.

Prettier can [ignore files/patterns specified in a `.prettierignore`
file][prettier-ignore-file-url]. Except for a few exceptions, all files that
are supported by Prettier are taken into account by default. The location of
the ignore file can be configured using the `PRETTIER_IGNORE` environment
variable in CI. Using [GitLab's _file_ environment
variables][gitlab-file-variable-url], the files/patterns to ignore can also be
configured through GitLab's UI.

Inclusion of the job in a pipeline can be prevented by setting the
`ENABLE_CODE_FORMATTING_PRETTIER` environment variable to any value other than
`"true"`.

### [GitLab Pages for Statically Defined Sites](/.gitlab/ci/gitlab-pages.yml)

The [`.gitlab/ci/gitlab-pages.yml`](/.gitlab/ci/gitlab-pages.yml) file defines
a CI job publishing a `public` folder committed to a branch, i.e. the job does
not run if such a folder does not exist. This is useful for both simple website
deployments maintaining a `public/` folder on the default branch, as well as
more complex deployment flows that maintain a dedicated 'deployment' branch,
e.g. a `gitlab-pages` branch.

By default, deployment occurs from the [default
branch][default-branch-docs-url] of a project. This behavior can be disabled by
setting the `PUBLISH_GITLAB_PAGES_FROM_DEFAULT_BRANCH` CI variable to any value
other than `"true"`.

Deploying from a different branch than the default can be achieved by
specifying that branch through the `GITLAB_PAGES_DEPLOYMENT_BRANCH` CI
variable, `GITLAB_PAGES_DEPLOYMENT_BRANCH="gitlab-pages"`.

## [Stages](/.gitlab/ci/stages.yml)

The [`.gitlab/ci/stages.yml`](/.gitlab/ci/stages.yml) file defines a set of
common [`stages`][gitlab-ci-stages-url] that CI jobs can be defined against. CI
jobs relying on these jobs may assume this file has been included through the
pipeline they are integrated into.

A more robust approach is for jobs to [conditionally include the
definitions][conditional-includes-docs-url], based on the
`GITLAB_CI_STAGES_INCLUDED` variable the file sets when it has been included.

## [Variables](/.gitlab/ci/variables.yml)

The [`.gitlab/ci/variables.yml`](/.gitlab/ci/variables.yml) file defines common
(environment) variables intended for reuse across Ethima jobs and processes.

Variables being defined include:

- a common Ethima Bot Git actor for creating commits, tags, etc.

## [Workflows](/.gitlab/ci/workflows.yml)

The [`.gitlab/ci/workflows.yml`](/.gitlab/ci/workflows.yml) file defines a hash
of common workflow scenarios under a `.Workflows` key. The keys of this hash
are meant to be included in [`workflow:rules`][gitlab-ci-workflow-rules-url]
definitions of processes controlling which jobs to include for given scenarios.
Typically by adjusting the `ENABLE_*` variables corresponding to the different
jobs.

The purpose of this approach is to facilitate modification of the different
scenarios across process definitions, with minimal code duplication. When
pipelines are created is typically governed by
[`workflow:rules`][gitlab-ci-workflow-rules-url], which is an array of hashes.
It is not possible to modify individual elements of an array, making extension
difficult. However, keys of hashes _are_ merged. This leads to a pattern of
hashes that _can_ be modified and then referenced in
[`workflow:rules`][gitlab-ci-workflow-rules-url] using [the `!reference`
tag][gitlab-ci-reference-tag-url].

As a concrete example consider the following process definition running the
[code formatting verification
(Prettier)](#code-formatting-verification-prettier) job from this project, but
only on merge requests, i.e. similar to how this process is configured.

<!-- BEGIN_VERSIONED_TEMPLATE

```yml
include:
  - file: "/.gitlab/ci/workflows.yml"
    project: "ethima/gitlab-ci"
    ref: "v__NEXT_SEMANTIC_RELEASE_VERSION__"
  - file: "/.gitlab/ci/verify-code-formatting-prettier.yml"
    project: "ethima/gitlab-ci"
    ref: "v__NEXT_SEMANTIC_RELEASE_VERSION__"

.My Process:
  extends: ".Workflows"
  Merge Request:
    variables:
      ENABLE_CODE_FORMATTING_PRETTIER: "true"
  workflow:
    rules:
      - !reference [.My Process, Primary Release Branch (Push)]
      - !reference [.My Process, Merge Request]

workflow: !reference [.My Process, workflow]
```

-->

```yml
include:
  - file: "/.gitlab/ci/workflows.yml"
    project: "ethima/gitlab-ci"
    ref: "v13.1.3"
  - file: "/.gitlab/ci/verify-code-formatting-prettier.yml"
    project: "ethima/gitlab-ci"
    ref: "v13.1.3"

.My Process:
  extends: ".Workflows"
  Merge Request:
    variables:
      ENABLE_CODE_FORMATTING_PRETTIER: "true"
  workflow:
    rules:
      - !reference [.My Process, Primary Release Branch (Push)]
      - !reference [.My Process, Merge Request]

workflow: !reference [.My Process, workflow]
```

<!-- END_VERSIONED_TEMPLATE_REPLACEMENT -->

Without the `.Workflows` definitions it would have been necessary to include
the entire definition of its `Primary Release Branch (Push)` and `Merge
Request` keys into the [`workflow:rules`][gitlab-ci-workflow-rules-url] here,
where a simple [`!reference`][gitlab-ci-reference-tag-url] now suffices.

As show in the example, it best to define the
[`workflow`][gitlab-ci-workflow-url] as part of the process' workflow and
reference it from the top-level [`workflow`][gitlab-ci-workflow-url]
definition. This ensures that other keys defined for the process-specific
[`workflow`][gitlab-ci-workflow-url], e.g.
[`workflow:name`][gitlab-ci-workflow-name-url], are included in the final
definition. Using this pattern also makes it straightforward for other
processes to build on top of this one.

The name to associate with pipelines created through processes building on the
`.Workflows` definitions can be controlled by defining a `PROCESS_NAME`
variable. The `PROCESS_NAME` defaults to the title of the project for which the
pipeline runs.

### Scenarios

The following workflow scenarios have been defined:

| Name                            | When                                                                                                     |
| ------------------------------- | -------------------------------------------------------------------------------------------------------- |
| `Maintenance Branch (Push)`     | Pushes to [maintenance branches][ethima-semantic-release-configuration-branches-url].                    |
| `Manual`                        | Triggered when a pipeline is created through the [_Run pipeline UI_][gitlab-manual-pipeline-url].        |
| `Merge Request`                 | Triggered by a merge request event.                                                                      |
| `Prerelease`                    | Pushes to tags that look like [semantic versions][semantic-version-url] for _prereleases_.               |
| `Prerelease Branch (Push)`      | Pushes to [prerelease branches][ethima-semantic-release-configuration-branches-url].                     |
| `Primary Release Branch (Push)` | Pushes to the [primary release branch][ethima-semantic-release-configuration-branches-url].              |
| `Release`                       | Pushes to tags that look like [semantic versions][semantic-version-url]. Does _not_ include prereleases! |
| `Schedule`                      | Triggered when a [scheduled pipeline][gitlab-ci-scheduled-pipeline-url] runs.                            |

[conditional-includes-docs-url]: https://docs.gitlab.com/ee/ci/yaml/includes.html#use-rules-with-include
[default-branch-docs-url]: https://docs.gitlab.com/ee/user/project/repository/branches/default.html
[dependency-management-usage-url]: https://gitlab.com/ethima/dependency-management#usage
[ethima-semantic-release-configuration-branches-url]: https://gitlab.com/ethima/semantic-release-configuration#branches
[gitlab-ci-reference-tag-url]: https://docs.gitlab.com/ee/ci/yaml/yaml_optimization.html#reference-tags
[gitlab-ci-scheduled-pipeline-url]: https://docs.gitlab.com/ee/ci/pipelines/schedules.html
[gitlab-ci-stages-url]: https://docs.gitlab.com/15.5/ee/ci/yaml/#stages
[gitlab-ci-workflow-name-url]: https://docs.gitlab.com/ee/ci/yaml/#workflowname
[gitlab-ci-workflow-rules-url]: https://docs.gitlab.com/15.5/ee/ci/yaml/#workflowrules
[gitlab-ci-workflow-url]: https://docs.gitlab.com/ee/ci/yaml/workflow.html
[gitlab-file-variable-url]: https://docs.gitlab.com/ee/ci/variables/#cicd-variable-types
[gitlab-manual-pipeline-url]: https://docs.gitlab.com/ee/ci/pipelines/#run-a-pipeline-manually
[gitlab-scheduled-pipeline-url]: https://docs.gitlab.com/ee/ci/pipelines/schedules.html
[prettier-configuration-file-url]: https://prettier.io/docs/en/configuration.html
[prettier-configuration-options-url]: https://prettier.io/docs/en/options.html
[prettier-ignore-file-url]: https://prettier.io/docs/en/ignore.html
[prettier-url]: https://prettier.io
[semantic-release-configuration-usage-url]: https://gitlab.com/ethima/semantic-release-configuration#usage
[semantic-version-url]: https://semver.org
